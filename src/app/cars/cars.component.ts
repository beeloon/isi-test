import { Component, OnInit } from '@angular/core';

import { Car }        from '../car';
import { CarService } from '../car.service';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {
  cars: Car[];
  selectedCar: Car;
  
  constructor(private carService: CarService) { }

  ngOnInit() {
    this.getCars();
  }

  onSelect(car: Car): void {
    this.selectedCar = car;
  }
  
  getCars(): void {
    this.cars = this.carService.getAllCars();
  }

  addCar(): void {
    let [cars, ln] = [this.cars, this.cars.length];
    let id = ln > 0 ? Math.max(...cars.map(hero => hero.id)) + 1 : 1;
    
    let newCar = new Car();
    newCar.id = id;

    this.selectedCar = newCar;
  }
}
