export enum CarType { M = 'Minivan', S = 'Sedan', B = 'Bus' }

export class Car {
  id: number;
  name: string;
  model: string;
  year: number;
  type: CarType;
}