import { Injectable } from '@angular/core';

import { Car }  from './car';
import { CARS } from './mock-cars';

@Injectable({
  providedIn: 'root'
})

export class CarService {
  lastId: number = 0;

  cars: Car[] = CARS;

  addCar(car: Car): CarService {
    if (!car.id) {
      car.id = ++this.lastId;
    }
    this.cars.push(car);
    return this;
  }

  updateCar(id: number, details: Object = {}): Car {
    let car = this.getCarById(id);
    if (!car) {
      return null;
    }
    Object.assign(car, details);
    return car;
  }

  getAllCars(): Car[] {
    return this.cars;
  }

  getCarById(id: number): Car {
    return this.cars
      .filter(car => car.id === id)
      .pop();
  }
}
