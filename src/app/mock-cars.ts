import { Car, CarType } from './car';

export const CARS: Car[] = [
  { 
    id: 11, 
    name: 'Perodua Alza',
    model: 'SE',
    year: 2014,
    type: CarType.M
  },
  { 
    id: 12, 
    name: 'Proton Saga',
    model: 'FLX',
    year: 2013,
    type: CarType.S
  },
  { 
    id: 13, 
    name: 'Toyota Corolla Altis',
    model: 'G',
    year: 2002,
    type: CarType.S
  },
  { 
    id: 14, 
    name: 'Honda Jazz',
    model: 'V',
    year: 2015,
    type: CarType.B
  },
  { 
    id: 15, 
    name: 'Volkswagen Jetta',
    model: 'TSI',
    year: 2012,
    type: CarType.S
  },
  { 
    id: 16, 
    name: 'Honda CR',
    model: 'V',
    year: 2013,
    type: CarType.S
  },
];
