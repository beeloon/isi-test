import { Component, OnInit, Input } from '@angular/core';

import { Car }      from '../car';
import { CarType }  from '../car';

@Component({
  selector: 'app-car-detail',
  templateUrl: './car-detail.component.html',
  styleUrls: ['./car-detail.component.css']
})

export class CarDetailComponent implements OnInit {
  @Input() car: Car;
  carTypes = CarType;
  
  ngOnInit() { }

  onClose(): void {
    this.car = null;
  }

  onKey(event): any {
    console.log(event.target.value);
  }

  onChange(event): any {
    console.log(event.target.value);
  }

  onSave(): void { }
}
